#include <stdio.h>

// Define the structure
typedef struct {
    char name[51]; // Student's name, max 50 characters
    char dob[11]; // Date of birth in YYYY-MM-DD format, max 10 characters
    char reg_num[7]; // Registration number, 6 numerical characters
    char prog_code[5]; // Program code, max 4 characters
    double annual_tuition; // Annual tuition, a non-zero positive number
} Student;

int main() {
    // Create an instance of Student
    Student s1;

    // Assign values to the fields
    strcpy(s1.name, "Firidaus Kagolo");
    strcpy(s1.dob, "2002-04-10"); 
    strcpy(s1.reg_num, "247");
    strcpy(s1.prog_code, "CSIT");
    s1.annual_tuition = 2000000.0;

    // Print the values
    printf("Name: %s\n", s1.name);
    printf("DOB: %s\n", s1.dob);
    printf("Registration Number: %06s\n", s1.reg_num);
    printf("Program Code: %s\n", s1.prog_code);
    printf("Annual Tuition: %.2f\n", s1.annual_tuition);


    return 0;
}
